package nodomain.selectoutline.config;

public enum OutlineColor {
	BLACK("Black"),
	WHITE("White"),
	RED("Red"),
	GREEN("Green"),
	BLUE("Blue"),
	PURPLE("Purple");

	private String displayText;

	OutlineColor(String displayText) {
		this.displayText = displayText;
	}

	public String getText() {
		return this.displayText;
	}

	public float getRed() {
		if (this.equals(WHITE))
			return 1;
		else if (this.equals(RED))
			return 1;
		else if (this.equals(PURPLE))
			return 0.5f;

		return 0;
	}

	public float getGreen() {
		if (this.equals(WHITE))
			return 1;
		else if (this.equals(GREEN))
			return 1;

		return 0;
	}

	public float getBlue() {
		if (this.equals(WHITE))
			return 1;
		else if (this.equals(BLUE))
			return 1;
		else if (this.equals(PURPLE))
			return 0.5f;

		return 0;
	}
}
