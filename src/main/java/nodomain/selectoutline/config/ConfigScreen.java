package nodomain.selectoutline.config;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.resource.language.I18n;

public class ConfigScreen extends Screen {
	private Screen parent = null;

	/* TODO: Remove this once we support not having an empty constructor */
	public ConfigScreen() {}

	public ConfigScreen(Screen parent) {
		this.parent = parent;
	}

	@Override
	public void init() {
		Config.load();

		this.buttons.add(new ButtonWidget(1, this.width / 2 - 100, 10, "Color: " + Config.color.getText()));
		this.buttons.add(new ButtonWidget(2, this.width / 2 - 100, 40, 20, 20, "<"));
		this.buttons.add(new ButtonWidget(3, this.width / 2 + 80, 40, 20, 20, ">"));
		this.buttons.add(new ButtonWidget(4, this.width / 2 - 100, 70, 20, 20, "<"));
		this.buttons.add(new ButtonWidget(5, this.width / 2 + 80, 70, 20, 20, ">"));
		this.buttons.add(new ButtonWidget(6, this.width / 2 - 100, 100, 20, 20, "<"));
		this.buttons.add(new ButtonWidget(7, this.width / 2 + 80, 100, 20, 20, ">"));
		this.buttons.add(new ButtonWidget(0, this.width-200-10, this.height-20-10, I18n.translate("gui.done")));
    }

	@Override
	protected void buttonClicked(ButtonWidget button) {
		if (button.id == 1) {
			Config.color = Config.color.values()[(Config.color.ordinal() + 1) % Config.color.values().length];
			button.message = "Color: " + Config.color.getText();
		}

		if (button.id == 2) {
			Config.width -= 1;
		}

		if (button.id == 3) {
			Config.width += 1;
		}

		if (button.id == 4) {
			Config.transparency -= 0.1;
		}

		if (button.id == 5) {
			Config.transparency += 0.1;
		}

		if (button.id == 6) {
			Config.fadeTime -= 0.5;
		}

		if (button.id == 7) {
			Config.fadeTime += 0.5;
		}


		if (Config.width > 10.01) Config.width = 10;
		if (Config.width < 0.99) Config.width = 1;

		if (Config.transparency > 1.01) Config.transparency = 1;
		if (Config.transparency < 0.09) Config.transparency = 0.1f;

		if (Config.fadeTime > 3.01) Config.fadeTime = 3;
		if (Config.fadeTime < -0.01) Config.fadeTime = 0;


		if (button.id == 0) {
			this.client.setScreen(this.parent);
		}
    }

	@Override
    public void render(int mouseX, int mouseY, float tickDelta) {
        this.renderBackground();
		this.drawCenteredString(this.textRenderer, "Width: " + (int)Math.round(Config.width), this.width / 2, 50 - (textRenderer.fontHeight/2), 0xFFFFFF);
		this.drawCenteredString(this.textRenderer, "Transparency: " + (int)Math.round(Config.transparency*100) + "%", this.width / 2, 80 - (textRenderer.fontHeight/2), 0xFFFFFF);
		this.drawCenteredString(this.textRenderer, "Fade Length: " + roundTo(Config.fadeTime, 1) + " seconds", this.width / 2, 110 - (textRenderer.fontHeight/2), 0xFFFFFF);
        super.render(mouseX, mouseY, tickDelta);
    }

	@Override
	public void removed() {
		Config.save();
    }

	private double roundTo(double value, int precision) {
		int scale = (int) Math.pow(10, precision);
		return (double) Math.round(value * scale) / scale;
	}
}
