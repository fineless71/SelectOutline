package nodomain.selectoutline;

import net.fabricmc.api.ModInitializer;
import nodomain.selectoutline.config.Config;

public class Main implements ModInitializer {
	@Override
	public void onInitialize() {
		Config.load();
	}
}
