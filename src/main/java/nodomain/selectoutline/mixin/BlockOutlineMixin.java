package nodomain.selectoutline.mixin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.lwjgl.opengl.GL11;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.block.material.Material;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.WorldRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResultType;
import nodomain.selectoutline.config.Config;
import nodomain.selectoutline.config.OutlineColor;

@Mixin(WorldRenderer.class)
public class BlockOutlineMixin {
	/* TODO: Cancel their code and mode all of it to game renderer to make the logic with fade more simple */
	/* TODO: Ignore above, use their code still for compatibility with other mods, and call it from the renderer */

	private final String outlineMethod = "drawBlockOutline(Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/util/hit/BlockHitResult;IF)V";

	@Inject(method = outlineMethod,
	at = @At("RETURN"))
	private void onOutline(PlayerEntity player, BlockHitResult hitResult, int i, float tickDelta, CallbackInfo info) {
		if (Config.fadeTime < 0.01) {
			Config.fades.clear();
			return;
		}

		if (i != 0 || hitResult.field_595 != HitResultType.BLOCK) return;
		if (MinecraftClient.getInstance().world.getBlock(hitResult.x, hitResult.y, hitResult.z).getMaterial() == Material.AIR) return;

		for (Map.Entry<BlockHitResult, Long> entry : Config.fades.entrySet()) {
			if (entry.getKey().x == hitResult.x && entry.getKey().y == hitResult.y && entry.getKey().z == hitResult.z)
				return;
		}

		Config.fades.put(hitResult, System.currentTimeMillis());
	}

	@Redirect(method = outlineMethod,
	at = @At(value = "INVOKE", target = "Lorg/lwjgl/opengl/GL11;glColor4f(FFFF)V"))
	private void changeColor(float red, float green, float blue, float alpha) {
		GL11.glColor4f(Config.color.getRed(), Config.color.getGreen(), Config.color.getBlue(), Config.transparency);
	}

	@Redirect(method = outlineMethod,
	at = @At(value = "INVOKE", target = "Lorg/lwjgl/opengl/GL11;glLineWidth(F)V"))
	private void changeWidth(float width) {
		/*LivingEntity player = MinecraftClient.getInstance().field_6279;
		BlockHitResult result = MinecraftClient.getInstance().result;
		float distance = (float) Math.sqrt(Math.pow(player.x - result.x, 2) + Math.pow(player.y - result.y, 2) + Math.pow(player.z - result.z, 2));*/
		GL11.glLineWidth(Config.width);
	}

	/*@ModifyConstant(method = outlineMethod)
	private float changeExpand(float value) {
		if (value == 0.002f)
			return 0.1f;
		return value;
	}*/
}
