package nodomain.selectoutline.mixin;

import java.util.Map;

import javax.security.auth.callback.Callback;

import org.lwjgl.opengl.GL11;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.mojang.blaze3d.platform.GLX;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.render.WorldRenderer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.world.World;
import nodomain.selectoutline.config.Config;

@Mixin(GameRenderer.class)
public class GameRendererMixin {
	@Inject(method = "renderWorld(FJ)V",
	at = @At(value = "INVOKE", target = "Lnet/minecraft/client/render/WorldRenderer;drawBlockOutline(Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/util/hit/BlockHitResult;IF)V"))
	private void onRenderWorld(float tickDelta, long limitTime, CallbackInfo info) {
		if (Config.fadeTime < 0.01) return;
		if (MinecraftClient.getInstance().result == null) return;

		LivingEntity player = MinecraftClient.getInstance().field_6279;
		World world = MinecraftClient.getInstance().world;

		Config.fades.entrySet().removeIf(entry -> System.currentTimeMillis()-entry.getValue() > Config.fadeTime*1000);

		/* TODO: Don't reimplement, just call the drawOutline */
		for (Map.Entry<BlockHitResult, Long> entry : Config.fades.entrySet()) {
			BlockHitResult hitResult = entry.getKey();
			//if (hitResult.equals(MinecraftClient.getInstance().result)) continue;
			if (hitResult.x == MinecraftClient.getInstance().result.x && hitResult.y == MinecraftClient.getInstance().result.y && hitResult.z == MinecraftClient.getInstance().result.z) continue;

			GL11.glEnable(3042);
            GLX.glBlendFuncSeparate(770, 771, 1, 0);
            GL11.glColor4f(Config.color.getRed(), Config.color.getGreen(), Config.color.getBlue(), (1f-((System.currentTimeMillis()-entry.getValue())/(Config.fadeTime*1000)))*Config.transparency);
            GL11.glLineWidth(Config.width);
            GL11.glDisable(3553);
            GL11.glDepthMask(false);
			float f = 0.002f;
			Block block = world.getBlock(hitResult.x, hitResult.y, hitResult.z);
			block.onRender(world, hitResult.x, hitResult.y, hitResult.z);
			double d = player.prevTickX + (player.x - player.prevTickX) * (double)tickDelta;
			double d2 = player.prevTickY + (player.y - player.prevTickY) * (double)tickDelta;
			double d3 = player.prevTickZ + (player.z - player.prevTickZ) * (double)tickDelta;
			WorldRenderer.method_6886(block.getRenderBoundingBox(world, hitResult.x, hitResult.y, hitResult.z).expand(f, f, f).method_592(-d, -d2, -d3), -1);
			GL11.glDepthMask(true);
            GL11.glEnable(3553);
            GL11.glDisable(3042);
		}
	}
}
